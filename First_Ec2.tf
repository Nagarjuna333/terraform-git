provider "aws" {
  region     = "us-west-2"
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_instance" "first_ec2" {
  ami           = "ami-0841edc20334f9287"
  instance_type = "t2.micro"

}
